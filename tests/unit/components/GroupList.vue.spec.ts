import { mount } from '@vue/test-utils'
import GroupList from '@/components/GroupList.vue'
import { Group } from '@/models/Group'

describe('GroupList', () => {
  const groups: Group[] = [
    {
      id: 'group1',
      title: 'Group 1',
      description: 'This is group 1',
      order: 0,
      techniques: []
    },
    {
      id: 'group2',
      title: 'Group 2',
      description: 'This is group 2',
      order: 1,
      techniques: []
    }
  ]

  it('renders the correct number of groups', () => {
    const wrapper = mount(GroupList, {
      props: {
        groups
      }
    })

    const groupElements = wrapper.findAll('li')
    expect(groupElements.length).toBe(groups.length)
  })

  it('renders the correct group title and description', () => {
    const wrapper = mount(GroupList, {
      props: {
        groups
      }
    })

    const firstGroupElement = wrapper.find('li:first-child')
    expect(firstGroupElement.text()).toContain(groups[0].title)
    expect(firstGroupElement.text()).toContain(groups[0].description)
  })
})