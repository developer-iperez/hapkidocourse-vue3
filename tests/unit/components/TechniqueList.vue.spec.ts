import { mount } from '@vue/test-utils'
import TechniqueList from '@/components/TechniqueList.vue'
import { Technique } from '@/models/Technique'

describe('TechniqueList.vue', () => {
  it('renders a list of techniques', () => {
    const techniques: Technique[] = [
      { description: 'Technique 1' },
      { description: 'Technique 2' },
      { description: 'Technique 3' }
    ]
    const wrapper = mount(TechniqueList, {
      props: {
        techniques
      }
    })
    const items = wrapper.findAll('li')
    expect(items).toHaveLength(techniques.length)
    items.forEach((item, index) => {
      expect(item.text()).toBe(techniques[index].description)
    })
  })
})