import { shallowMount } from '@vue/test-utils'
import SubgroupList from '@/components/SubgroupList.vue'
import { Subgroup } from '@/models/Subgroup'

describe('SubgroupList.vue', () => {
  it('renders a list of subgroups', () => {
    const programId = 'program-id'
    const groupId = 'group-id'
    const subgroups: Subgroup[] = [
      {
        id: 'subgroup-1',
        title: 'Subgroup 1',
        description: 'This is the description for Subgroup 1',
        order: 0,
        techniques: []
      },
      {
        id: 'subgroup-2',
        title: 'Subgroup 2',
        description: 'This is the description for Subgroup 2',
        order: 1,
        techniques: []
      }
    ]
    const wrapper = shallowMount(SubgroupList, {
      props: {
        programId,
        groupId,
        subgroups
      }
    })
    expect(wrapper.findAll('li')).toHaveLength(2)
  })
})