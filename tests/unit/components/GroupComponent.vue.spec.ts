import { shallowMount } from '@vue/test-utils'
import GroupComponent from '@/components/GroupComponent.vue'
import TechniqueList from '@/components/TechniqueList.vue'
import SubgroupList from '@/components/SubgroupList.vue'
import { Program } from '@/models/Program'
import { Group } from '@/models/Group'

describe('GroupComponent.vue', () => {
  it('renders group title and description', () => {
    const group: Group = {
      id: '1',
      title: 'Test Group',
      description: 'This is a test group',
      order: 1,
      techniques: [],
      subgroups: []
    }

    const program: Program = {
      id: '1',
      title: 'Test Program',
      level: 'basic',
      order: 1,
      groups: [group]
    }

    const wrapper = shallowMount(GroupComponent, {
      props: {
        program: program,
        group: group
      },
      global: {
        components: {
          TechniqueList,
          SubgroupList
        }
      }
    })

    expect(wrapper.text()).toContain('Test Group')
    expect(wrapper.text()).toContain('This is a test group')
  })
})