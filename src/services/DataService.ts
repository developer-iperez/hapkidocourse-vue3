import { Program } from "@/models/Program"
import programs from '@/data/Programs'

export default class DataService {
    getPrograms(): Program[] {
        return programs
    }

    getProgram(id: string): Program {
        const program = programs.find(x => x.id === id) as Program
        return program
    }
}