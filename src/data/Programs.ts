import { Program } from "@/models/Program"
import white from './01_hapkido_white_v1.json'

const programs: Array<Program> = [
  white
]

export default programs