import { Technique } from "./Technique"

export class Subgroup {
    public id: string
    public title: string
    public description: string
    public order: number
    public techniques: Technique[]

    constructor(
        id: string,
        title: string,
        description: string,
        order: number,
        techniques: Technique[]
    ) {
        this.id = id
        this.title = title
        this.description = description
        this.order = order
        this.techniques = techniques
    }
}