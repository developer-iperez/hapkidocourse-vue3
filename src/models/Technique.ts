export interface Technique {
    description: string
  }
  
  export class Technique implements Technique {
    constructor(public description: string) {}
  }