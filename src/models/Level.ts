import { Program } from "./Program"

export interface Level {
    id: string
    title: string
    color?: string
    programs?: Program[]
  }
  
  export class Level implements Level {
    constructor(public id: string, public title: string, public color?: string, public programs?: Program[]) {}
  }