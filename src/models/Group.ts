import { Subgroup } from './Subgroup'
import { Technique } from './Technique'

export class Group {

    public id: string
    public title: string
    public description?: string
    public order: number
    public techniques: Technique[]
    public subgroups?: Subgroup[] 

    constructor(
        id: string,
        title: string,
        description: string,
        order: number,
        techniques: Technique[],
        subgroups?: Subgroup[]
    ) {
        this.id = id
        this.title = title
        this.order = order
        this.techniques = techniques
        this.description = description
        this.subgroups = subgroups || []
    }
}