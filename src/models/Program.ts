import { Group } from './Group'

export class Program {
  public id: string
  public title: string
  public level: string
  public order: number
  public groups: Group[]

  constructor(id: string, title: string, level: string, order: number, groups: Group[]) {
    this.id = id
    this.title = title
    this.level = level
    this.order = order
    this.groups = groups
  }
}